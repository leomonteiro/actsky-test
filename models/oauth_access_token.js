const { Schema, model } = require("mongoose");

const schema = new Schema({
  accessToken: {
    required: true,
    type: String
  },
  accessTokenExpiresAt: {
    required: true,
    type: Date
  },
  client: {
    required: true,
    type: Schema.Types.ObjectId,
    ref: "OAuth2Client"
  },
  user: {
    required: true,
    type: Schema.Types.ObjectId,
    ref: "User"
  }
});

const ModelImpl = model("OAuth2AccessToken", schema);

module.exports = ModelImpl;
