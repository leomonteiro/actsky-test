const { Schema, model } = require("mongoose");

const OAuth2ClientSchema = new Schema({
  id: {
    required: true,
    type: String
  },
  secret: {
    required: true,
    type: String
  },
  grants: {
    required: true,
    type: [String]
  }
});

const ModelImpl = model("OAuth2Client", OAuth2ClientSchema);

module.exports = ModelImpl;
