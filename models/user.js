const { Schema, model } = require("mongoose");
const bcrypt = require("bcrypt");

const userSchema = new Schema({
  nome: {
    required: true,
    type: String
  },
  email: {
    index: true,
    lowercase: true,
    required: true,
    trim: true,
    type: String,
    unique: true
  },
  senha: String,
  telefones: [
    {
      numero: Number,
      ddd: Number
    }
  ],
  data_criacao: {
    type: Date,
    default: Date.now
  },
  data_atualizacao: {
    type: Date,
    default: Date.now
  },
  ultimo_login: {
    type: Date,
    default: Date.now
  },
  token: String
});

userSchema.pre("save", function() {
  const salt = bcrypt.genSaltSync(10);
  this.senha = bcrypt.hashSync(this.senha, salt);
});

userSchema.pre("update", function() {
  this.data_atualizacao = new Date();
});

const ModelImpl = model("User", userSchema);

ModelImpl.prototype.verifyPassword = function(password) {
  return bcrypt.compareSync(password, this.senha);
};

module.exports = ModelImpl;
