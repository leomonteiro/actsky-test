const { UserService } = require("../services");
const Validator = require("fastest-validator");

module.exports.signUp = async (req, res) => {
  const v = new Validator();
  const schema = {
    nome: "string",
    email: "email",
    senha: "string",
    telefones: {
      type: "array",
      min: 1,
      items: {
        type: "object",
        props: {
          ddd: "any",
          numero: "any"
        }
      }
    },
    $$strict: true
  };
  try {
    await v.validate(req.body, schema);
    const exists = await UserService.findOne({ email: req.body.email });
    if (!!exists) {
      res.status(400);
      return res.json({ message: "E-mail já existe" });
    }
    const result = await UserService.create(req.body);
    result.senha = undefined; // Remover a senha do retorno da resposta
    res.status(201);
    return res.json(result);
  } catch (err) {
    console.error(err);
    res.status(400);
    return res.json({ message: err.message });
  }
};

module.exports.details = async (req, res) => {
  const v = new Validator();
  const schema = {
    _id: "string",
    email: "email",
    $$strict: true
  };
  try {
    const { user: authenticatedUser } = res.locals.oauth.token;
    if (req.params._id !== authenticatedUser._id.toString()) {
      console.warn("Não autorizado");
      res.status(401);
      return res.end();
    }
    await v.validate({ ...req.query, ...req.params }, schema);
    const result = await UserService.findById(req.params._id);
    if (!result) throw new Error(`Usuário não encontrado`);
    result.senha = undefined;
    res.status(200);
    return res.json(result);
  } catch (err) {
    console.error(err);
    res.status(400);
    return res.json({ message: err.message });
  }
};
