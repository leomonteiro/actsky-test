const OAuth2Server = require('express-oauth-server');
const { OAuthClient, OAuthAccessToken, User } = require('../models');
const jwt = require('jsonwebtoken');

async function generateAccessToken(cl, user, scope) {
  return jwt.sign(
    {
      _id: user._id
    },
    process.env.JWT_SECRET || 'actsky',
    {
      expiresIn: 60 * 30
    }
  );
}

async function getAccessToken(token) {
  const accessToken = await OAuthAccessToken.findOne({
    accessToken: token
  })
    .populate('client')
    .populate('user')
    .exec();
  if (!accessToken) {
    throw new Error(`token ${token} não encontrado`);
  }
  return accessToken;
}

async function getClient(clientId, clientSecret) {
  const client = await OAuthClient.findOne({
    id: clientId,
    secret: clientSecret
  }).exec();
  if (!client) {
    throw new Error(`client ${clientId} não encontrado`);
  }
  return client;
}

async function getUser(username, password) {
  const user = await User.findOne({
    email: username
  }).exec();
  if (!user) {
    throw new Error(`usuário não encontrado com e-mail ${username}`);
  }
  if (!user.verifyPassword(password)) {
    throw new Error(`senha incorreta`);
  }
  await user.update({ ultimo_login: new Date() });
  user.password = undefined;
  return user;
}

async function saveToken(token, cl, user) {
  let accessToken = new OAuthAccessToken({
    ...token,
    client: cl,
    user
  });
  accessToken = await accessToken.save();
  return accessToken;
}

const OAuthProvider = new OAuth2Server({
  model: {
    generateAccessToken,
    getAccessToken,
    getClient,
    getUser,
    saveToken
  },
  accessTokenLifetime: 60 * 1000
});

module.exports = OAuthProvider;
