const chai = require('chai');
const chaiHttp = require('chai-http');
const { expect } = chai;
const app = require('../index');
const jwt = require('jsonwebtoken');
const { User, OAuthAccessToken, OAuthClient } = require('../models');

chai.use(chaiHttp);

const testUser1 = {
  nome: 'Leonardo Monteiro',
  email: 'leomonteiro92+1@gmail.com',
  senha: '123456',
  telefones: [
    {
      ddd: '11',
      numero: '949496404'
    }
  ]
};

const testUser2 = {
  nome: 'Leonardo Monteiro',
  email: 'leomonteiro92+2@gmail.com',
  senha: '123456',
  telefones: [
    {
      ddd: '11',
      numero: '949496404'
    }
  ]
};

beforeEach(async () => {
  await User.deleteMany();
});

describe('#Sign Up', () => {
  it('Deve retornar status 201 com o usuário recém-criado', done => {
    chai
      .request(app)
      .post('/users/signup')
      .send(testUser1)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(201);
        expect(res.body).to.haveOwnProperty('_id');
        expect(res.body).to.haveOwnProperty('nome');
        expect(res.body).to.haveOwnProperty('email');
        expect(res.body).to.haveOwnProperty('telefones');
        expect(res.body).to.haveOwnProperty('data_criacao');
        expect(res.body).to.haveOwnProperty('data_atualizacao');
        expect(res.body).to.haveOwnProperty('ultimo_login');
        expect(res.body).to.not.haveOwnProperty('senha');
        done();
      });
  });

  it('Deve retornar status 400 caso o e-mail já exista', done => {
    User.create(testUser1)
      .then(() => {
        chai
          .request(app)
          .post('/users/signup')
          .send(testUser1)
          .end((err, res) => {
            expect(res).to.have.status(400);
            expect(res.body).to.haveOwnProperty('message');
            expect(res.body.message).to.be.eq('E-mail já existe');
            done();
          });
      })
      .catch(done);
  });
});

describe('#Sign In', () => {
  it('Deve autenticar um usuário', done => {
    const user = new User(testUser1);
    user
      .save()
      .then(() => {
        chai
          .request(app)
          .post('/users/signin')
          .set('Content-Type', 'application/x-www-form-urlencoded')
          .send({
            username: testUser1.email,
            password: testUser1.senha,
            grant_type: 'password',
            client_id: 'actsky',
            client_secret: 'actsky'
          })
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res.body).to.haveOwnProperty('access_token');
            done();
          });
      })
      .catch(done);
  });

  it('Deve informar que um usuário não existe', done => {
    const user = new User(testUser1);
    user
      .save()
      .then(() => {
        chai
          .request(app)
          .post('/users/signin')
          .set('Content-Type', 'application/x-www-form-urlencoded')
          .send({
            username: testUser2.email,
            password: testUser2.senha,
            grant_type: 'password',
            client_id: 'actsky',
            client_secret: 'actsky'
          })
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(503);
            expect(res.body).to.haveOwnProperty('error_description');
            expect(res.body.error_description).to.be.eq(
              `usuário não encontrado com e-mail ${testUser2.email}`
            );
            done();
          });
      })
      .catch(done);
  });

  it('Deve informar caso a senha passada seja incorreta', done => {
    const user = new User(testUser1);
    user
      .save()
      .then(() => {
        chai
          .request(app)
          .post('/users/signin')
          .set('Content-Type', 'application/x-www-form-urlencoded')
          .send({
            username: testUser1.email,
            password: '1234567', //senha incorreta
            grant_type: 'password',
            client_id: 'actsky',
            client_secret: 'actsky'
          })
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(503);
            expect(res.body).to.haveOwnProperty('error_description');
            expect(res.body.error_description).to.be.eq(`senha incorreta`);
            done();
          });
      })
      .catch(done);
  });
});

describe('#Detalhes do usuário', () => {
  it('Deve permitir acesso para o id cujo token tenha sido gerado e não permitir que outro usuário tenha acesso', done => {
    OAuthClient.findOne({
      id: 'actsky'
    })
      .then(cl => {
        const user = new User(testUser1);
        const accessToken = new OAuthAccessToken({
          accessToken: jwt.sign({ _id: user._id }, 'actsky'),
          accessTokenExpiresAt: '2019-12-30',
          client: cl._id,
          user: user._id
        });
        return Promise.all([user.save(), accessToken.save()]);
      })
      .then(([user, token]) => {
        chai
          .request(app)
          .get(`/users/info/${user._id}`)
          .set('Authorization', `Bearer ${token.accessToken}`)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res.body).to.haveOwnProperty('_id');
            expect(res.body).to.haveOwnProperty('nome');
            expect(res.body).to.haveOwnProperty('email');
            expect(res.body).to.haveOwnProperty('telefones');
            expect(res.body).to.haveOwnProperty('data_criacao');
            expect(res.body).to.haveOwnProperty('data_atualizacao');
            expect(res.body).to.haveOwnProperty('ultimo_login');
            expect(res.body).to.not.haveOwnProperty('senha');
            done();
          });
      });
  });
});

after(() => {
  process.exit(0);
});
