const { User } = require("../models");

module.exports.create = async inputUser => {
  let user = new User(inputUser);
  user = await user.save();
  return user;
};

module.exports.patch = async (id, inputUser) => {
  let user = await User.findByIdAndUpdate(id, { $set: inputUser });
  if (!user) {
    throw new Error(`usuário não encontrado com id ${id}`);
  }
  return user;
};

module.exports.findById = async id => {
  let user = await User.findById(id);
  if (!user) {
    throw new Error(`usuário não encontrado com id ${id}`);
  }
  return user;
};

module.exports.findOne = async filters => {
  let user = await User.findOne(filters);
  return user;
};
