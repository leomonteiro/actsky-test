const express = require("express");
const bodyParser = require("body-parser");

const PORT = process.env.PORT || 5000;
const app = express();
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

require("./config/datasource"); // Connect to database
require("./routes")(app); // Bind routes

app.listen(PORT, () => {
  console.log(`Server up and running on port ${PORT}`);
});

module.exports = app;
