const { Router } = require("express");
const { UserController, OAuthController } = require("../controllers");

const router = Router();

router.post("/signup", UserController.signUp);

router.post("/signin", OAuthController.token());

router.get(
  "/info/:_id",
  OAuthController.authenticate(),
  UserController.details
);

module.exports = router;
