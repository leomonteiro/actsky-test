const { OAuthClient } = require('./models');

require('./config/datasource');

let record;

OAuthClient.findOne({ id: 'actsky', secret: 'actsky ' })
  .then(exists => {
    if (!exists) {
      record = new OAuthClient({
        id: 'actsky',
        secret: 'actsky',
        grants: ['password', 'refresh_token']
      });
      return record.save();
    }
    console.info('Client already seeded');
    return exists;
  })
  .then(console.info)
  .catch(console.error)
  .finally(() => {
    process.exit(0);
  });
